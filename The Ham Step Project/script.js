'use strict';

let currentCategory = "all";

const grid_images = document.querySelectorAll(".grid_images");

const container = document.querySelector(".our_amazing_work_catagories");
console.log(container);

container.addEventListener('click', (event) => {
    currentCategory = event.target.dataset.category;
    
    grid_images.forEach(item => {
        // item.setAttribute("hidden", true);
        console.log(item.dataset.category);
        
        if (item.dataset.category === currentCategory || currentCategory === "all") {
            // item.setAttribute("hidden", false);
            item.style.display="block";
        } else {
            item.style.display="none";
        }
    })
    
    const our_amazing_work_catagory_cell = document.querySelectorAll(".our_amazing_work_catagory_cell");
    our_amazing_work_catagory_cell.forEach(elem => {
        elem.style.background="#E9EDEF";


        if(elem.dataset.category === currentCategory) {
            elem.style.background="#18CFAB";
        }
    })

    const active = document.querySelectorAll(".active");
    active.forEach(elem => {
        elem.style.background="#E9EDEF";

        if(elem.dataset.category === currentCategory) {
            elem.style.background="#18CFAB";
        }
    })
});

let particularCategory = "our_services";

const content = document.querySelector(".our_services_catagories");
console.log(content);

content.addEventListener('click', (event) => {
    particularCategory = event.target.dataset.category;

    const image_block = document.querySelectorAll(".our_services_image_block");
    image_block.forEach(elem => {
        // elem.classList.remove("vision");
        elem.style.display="none";

        if (elem.dataset.category === particularCategory) {
            elem.style.display="flex";
        }
    })

    const table_cell = document.querySelectorAll(".table_cell");
    table_cell.forEach(elem => {
        elem.style.background="#F8FCFE";

        if (elem.dataset.category === particularCategory) {
            elem.style.background="#18CFAB";
        }
    })

    const web_design_space = document.querySelectorAll(".web_design_space");
    web_design_space.forEach(elem => {
        elem.style.background="#F8FCFE";

        if(elem.dataset.category === particularCategory) {
            elem.style.background="#18CFAB";
        }
    })

});

// $('.client_feedback_images') ({
//     infinite: true,
//     slidesToShow: 3,
//     slidesToScroll: 3,
//     prevArrow: '<button type="button" data-role="none" class="slick-prev">Previous</button>',
//     autoplay: false,
//     autoplaySpeed: 3000,
//   });

// const category_images = 